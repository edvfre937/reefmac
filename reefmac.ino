/*
  Created by: Edvin Nordholm
  2019-01-02
  version 1.0
  ReefMac (aqurium controller with arduino)
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K potentiometer:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
*/

// include the library code:
#include <LiquidCrystal.h>



// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
const int relay=10;
int state=0;
const int buttonPin = 7;
const int buttonPin2 = 8;
int buttonState = 0;
int buttonState2 = 0; 

void setup() {
  
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    // Print a message to the LCD.
    // lcd.print("REEFMAC");
    pinMode(relay,OUTPUT);
    digitalWrite(relay,HIGH);
    pinMode(buttonPin, INPUT);
    pinMode(buttonPin2, INPUT);
}

void loop() {
  
  buttonState = digitalRead(buttonPin);
  buttonState2 = digitalRead(buttonPin2);
  lcd.setCursor(1, 0);
   { lcd.print("REEFMAC");
   }
   
  // set the cursor to column 0, line 1
  lcd.setCursor(0, 1);
   {
  if(Serial.available() > 0){ // Checks whether data is comming from the serial port
    state = Serial.read(); // Reads the data from the serial port
 }
 
 if (state == '0') {
    digitalWrite(relay, LOW); // Turn RELAY OFF
    lcd.print("PUMP: OFF ");
    Serial.println("PUMP: OFF "); 
    state = 0;
    lcd.print("                ");
 }
 
 else if (state == '1') {
    digitalWrite(relay, HIGH); // Turn Relay ON
    lcd.print("PUMP: ON");
    Serial.println("PUMP: ON");
    state = 0;
    lcd.print("                ");
 }
  
 else if(state == '3'){
    digitalWrite(relay, LOW);
    lcd.print("FEED MODE: ON");
    Serial.println("FEED MODE: ON");
    delay(10000); 
    state = 0;
    lcd.print("                ");
 }
 
 else if(buttonState == HIGH){ //Button press Relay ON
    digitalWrite(relay, HIGH);
    lcd.print("PUMP: ON ");
    Serial.println("PUMP: ON");
}

 else if(buttonState2 == HIGH){ //Button press Relay OFF
    digitalWrite(relay, LOW);
    lcd.print("PUMP: OFF");
    Serial.println("PUMP: OFF");

    }
   }
}